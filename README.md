# Kether.pl L4D2 Server Files
##### Contains server files for my L4D2 community. Files were done basing on [Sir Please Competitive Rework](https://github.com/SirPlease/L4D2-Competitive-Rework). <br/>You can find an instruction how to install these files in the above link.

##### An additional information is included in the [general_info.txt](general_info.txt) file.
