**Kether's Custom changes are applied to the selected maps:**
- c4m1-4: *Removed pills spawns*
- c5m1: *-,,-*
- c8m3: *Golden sedan car in front of the restaurant*
- c10m1: *Reverted cliff top block*
- c13m3: *Reverted insta-blocking fences and trailers*

**Custom maps:**
- cbm1, cbm2: *Fixed broken SI ladders*