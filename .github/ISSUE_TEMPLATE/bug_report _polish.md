---
name: Zgłoszenie błędu [PL]
about: Coś nie działa? Napisz nam o tym.
title: "[BUG]"
labels: ''
assignees: ''

---

**Opisz błąd**
Jasny i zwięzły opis z czym występuje problem.

**Reprodukcja**
Kroki w celu reprodukcji opisywanego błędu:
1. Idź do '...'
2. Kliknij '....'
3. Zjedź do '....'
4. Tu zobaczysz błąd

**Zrzuty ekranu/Wideo**
W razie potrzeby dodaj zrzuty ekranu i/lub film, aby wyjaśnić problem.

**System operacyjny (jeśli dotyczy):**
 - Linux/Windows

**Dodatkowe informacje**
Dodaj tutaj dowolny inny kontekst dotyczący problemu.
