---
name: Prośba o funkcję [PL]
about: Sugestie nowych funkcji, zmian funkcji, itd.
title: "[Request]"
labels: ''
assignees: ''

---

**Czy Twoja prośba o dodanie funkcji jest związana z problemem? Proszę opisz.**
Jasny i zwięzły opis problemu. Np. Zawsze mnie wkurza gdy [...]

**Opisz najlepsze wg. Ciebie rozwiązanie**
Jasny i zwięzły opis tego, co chcesz, by się wydarzyło.

**Opisz alternatywy które rozważałeś**
Jasny i zwięzły opis wszelkich alternatywnych rozwiązań lub funkcji, które brałeś pod uwagę.

**Więcej informacji**
Napisz tu coś jeszcze, lub załącz zrzuty ekranu dotyczące prośby o funkcję.

